#!/usr/bin/env python
# coding: utf-8

# In[4]:


from random import randint
import sys

print("Hi, What is your name?")
name = input()

if name != "":
    print("Hi " + name + ", I am thinking of an integer between 1 and 20. Can you guess it? You will get 5 guesses.")
else:
    print("You didn't enter anything.")
    sys.exit()
    

number = randint(1,20)
for i in range(5):
    print("Take a guess:")
    guess = input()
    if number - int(guess) <= -5:
        print("Too hot.")
        continue
    elif number - int(guess) >= 5:
        print("Too cold.")
        continue
    elif number - int(guess) <= -1:
        print("Hot.")
        continue
    elif number - int(guess) >= 1:
        print("Cold.")
    else:
        print("Ding! Ding! Ding!")
        break
        
print("The correct answer was " + str(number) + ".")


# In[ ]:




