from sigmoid import *
from feature_normalize import *
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

def costlogisticReg(x,y,theta,lmbda):
    temptheta = theta
    temptheta[0] = 0
    temp = np.sum((-y * np.log(sigmoid(np.dot(x,theta)))) - ((1-y)*np.log(1 - sigmoid(np.dot(x,theta))))) / m + ((lmbda / (2*m))*np.sum(temptheta**2))

    return temp

def gradientDescentReg(X,y,theta,alpha,lmbda,num_iters):
    
    for i in range(num_iters):
        temptheta = theta
        temptheta[0] = 0
        
        predictions = sigmoid(X.dot(theta))
        error = np.dot(X.transpose(),(predictions -y))
        descent=1/m * error + ((lmbda / m)*np.sum(temptheta))
        theta-= alpha * descent
    
    return theta

def mapFeatures(x):
    x1 = x[:,0].reshape((m,1))
    x2 = x[:,1].reshape((m,1))

    out = np.ones((m,1))
    degree = 6
    for i in range(1,degree+1):
        for j in range(0,i+1):
            out = np.append(out,(x1**(i-j))*(x2**j),axis = 1)

    return out[:,1:]
    
df = pd.read_csv("ex2data2.txt", header=None)
dfn = df.values
m,n = dfn.shape
x = dfn[:,:n-1]
y = dfn[:,n-1].reshape((m,1))
x = mapFeatures(x)

x, mu, sigma = feature_normalize(x)
x = np.append(np.ones((m,1)),x,axis=1)

theta = np.zeros((x.shape[1],1))

print(costlogisticReg(x,y,theta,1))

theta = gradientDescentReg(x,y,theta,0.2,1,800)

print(costlogisticReg(x,y,theta,1))

print("\n\n\nTesting the accuracy of the training")
pred = x.dot(theta) > 0
print(str(sum(pred == y)[0]) + "% accurate")
