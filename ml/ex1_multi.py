# importing the libraries
import numpy as np
import pandas as pd
from feature_normalize import *

from matplotlib import pyplot as plt

# defining the cost function, which will be minimised later
def compute_cost(x,y,theta):
    error = (x.dot(theta) - y)**2
    return 1/(2*m) * np.sum(error)

def gradientDescent(X,y,theta,alpha,num_iters):
    """
    Take in numpy array X, y and theta and update theta by taking   num_iters gradient steps
    with learning rate of alpha
    
    return theta and the list of the cost of theta during each  iteration
    """
    for i in range(num_iters):
        predictions = X.dot(theta)
        error = np.dot(X.transpose(),(predictions -y))
        descent=alpha * 1/m * error
        theta-=descent
    
    return theta

df = pd.read_csv("ex1data2.txt", header = None) #importing the csv
dfn = df.values #converting the dataframe to np array
m,n = dfn.shape
x_init = dfn[:,:n-1].reshape((m,n-1))   # extract the x &
y = dfn[:,n-1:].reshape((m,1))          # the y component to be used as input and output

x , mu, sigma = feature_normalize(x_init)
x = np.append(np.ones((m,1)),x,axis=1) #adding a column of ones to x to accomodate theta0
theta = np.zeros((n,1)) #initialize theta

J = compute_cost(x,y,theta)
print(J)

theta = gradientDescent(x,y,theta,0.01,1500)
print(theta)

print("Enter sq ft of house")
sqft = input()
print("Enter number of rooms")
rooms = input()
temp = np.array([[float(sqft), float(rooms)]])
temp = (temp-mu)/sigma
temp = np.append(np.ones((1,1)),temp,axis=1)

print(np.dot(temp,theta))
