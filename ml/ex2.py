from sigmoid import *
from feature_normalize import *
import numpy as np
import pandas as pd

def costlogistic(x,y,theta):
    temp = (-y * np.log(sigmoid(np.dot(x,theta)))) - ((1-y)*np.log(1 - sigmoid(np.dot(x,theta))))

    return np.sum(temp) / m

def gradientDescent(X,y,theta,alpha,num_iters):
    
    for i in range(num_iters):
        predictions = sigmoid(X.dot(theta))
        error = np.dot(X.transpose(),(predictions -y))
        descent=alpha * 1/m * error
        theta-=descent
    
    return theta
    
df = pd.read_csv("ex2data1.txt", header=None)
dfn = df.values
m,n = dfn.shape
x = dfn[:,:n-1]
y = dfn[:,n-1].reshape((m,1))

x, mu, sigma = feature_normalize(x)
x = np.append(np.ones((m,1)),x,axis=1)
theta = np.zeros((n,1))

print(costlogistic(x,y,theta))
print(gradientDescent(x,y,theta,1,400))

x_test = np.array([45,85])
x_test = (x_test - mu)/sigma
x_test = np.append(np.ones(1),x_test)
prob = sigmoid(x_test.dot(theta))
print("For a student with marks 45 and 85, the pass probability is " + str(prob[0]*100) + "%")

print("\n\n\nTesting the accuracy of the training")
pred = x.dot(theta) > 0
print(str(sum(pred == y)[0]) + "% accurate")
