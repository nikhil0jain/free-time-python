# importing the libraries
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

# defining the cost function, which will be minimised later
def compute_cost(x,y,theta):
    m = len(y)
    error = (x.dot(theta) - y)**2
    return 1/(2*m) * np.sum(error)

def gradient(x,y,theta, alpha, iter):
    m = len(y)
    for i in range(1, iter + 1):
        theta = theta - ((alpha / m)*x.transpose().dot((x.dot(theta) - y)))

    return theta

df = pd.read_csv("ex1data1.txt", header = None) #importing the csv
dfn = df.values #converting the dataframe to np array
m = len(dfn)
x_init = dfn[:,0].reshape((m,1))    # extract the x &
y = dfn[:,1].reshape((m,1))         # the y component to be used as input and output

plt.scatter(x_init,y)
plt.show() #sample plot of the data

x = np.append(np.ones((m,1)),x_init,axis=1) #adding a column of ones to x to accomodate theta0
theta = np.zeros((2,1)) #initialize theta

J = compute_cost(x,y,theta)
print(J)

theta = gradient(x,y,theta,0.01,1500)
print(theta)

plt.scatter(x_init,y)
x_values = [x for x in range(25)]
y_values = [theta[0]+x*theta[1] for x in range(25)]
plt.plot(x_values,y_values,color = "red")
plt.xlabel("Population")
plt.ylabel("Revenue")
plt.show()

pop = 75000
predict = np.array([[1, pop]]).dot(theta)

print(predict)
