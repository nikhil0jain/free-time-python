import sys
birthdays = {}

while True:
    print("Enter a name (blank to exit)")
    a = input()
    if a == "":
        sys.exit()
    else:
        birthdays.setdefault(a,0)
        if birthdays[a] == 0:
            print("I don't have their birthday. Please enter so that I can store it for you")
            b = input()
            birthdays[a] = b
            print("Saved.")

        print(birthdays[a])
