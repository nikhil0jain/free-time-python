import random

def check_winner(user_in, com_in):
    if user_in == com_in:
        print("Computer chose " + com_choice)
        print("It's a draw.")
    elif user_in - com_in == -2 or user_in - com_in == 1:
        print("Computer chose " + com_choice)
        print("You won.")
    else:
        print("Computer chose " + com_choice)
        print("Computer won.")

def user_calc(user):
    global user_choice
    if "r" in user[0:3]:
        user_choice = 1
    elif "p" in user[0:3]:
        user_choice = 2
    elif "s" in user[0:3]:
        user_choice = 3
    return user_choice

def com_calc(computer):
    global com_choice
    if computer == 1:
        com_choice = "Rock"
    elif computer == 2:
        com_choice = "Paper"
    elif computer == 3:
        com_choice = "Scissors"
    return com_choice
        
print("Rock Paper Scissors")

print("Choose between Rock, Paper or Scissors")

user = (input()).lower()
user_choice = user_calc(user)
computer = random.randint(1,3)
com_calc(computer)
check_winner(user_choice, computer)
#Taking  1 for rock
#        2 for paper
#        3 for scissors
