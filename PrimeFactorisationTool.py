# This program calculates the Prime Factorisation of a number

import math
primes = []

def getPrimes(n):
    # Uses the sieve method to get the primes in first n positive integers
    global primeValidator
    primeValidator = [True for i in range(n)]
    p = 2

    while p**2 <= n:
        if primeValidator[p]:
            for a in range(p**2, n, p):
                primeValidator[a] = False
        p = p + 1

    for i in range(2,n):
        if primeValidator[i]:
            primes.append(i)

def calculate(number,upto):
    # Calculates the factors of the number and stores them in an array
    while multList(factors) < number1:
        a = getFactor(number)
        if a != 0:
            factors.append(a)
            number = number / a
            if number != 1:
                spam.append(int(number))
        else:
            if upto < math.ceil(number**0.5):
                addPrimes(upto,1000)
                upto += 1000
                print("Finding factors in primes upto " + str(upto) + "...")
                number = calculate(number, upto)
            break
    return number

def addPrimes(initial, howmany):
    # adds additional primes to the primes list
    # the howmany should be less than the initial getPrimes attribute
    primeValidator = []
    primeValidator = [True for i in range(howmany)]
    
    for p in primes:
        if p**2 <= initial+howmany:
            for a in range(howmany):
                if primeValidator[a] == True and (a+initial+1) % p == 0:
                    for i in range(a, howmany, p):
                        if primeValidator[i] == True:
                            primeValidator[i] = False
        

    for i in range(howmany):
        if primeValidator[i]:
            primes.append(i+initial+1)

def multList(anyList):
    # function to multiply numbers in a list, used to keep track of the factors
    result = 1
    for i in anyList:
        result = result * i
    return result

def getFactor(number):
    # tries to find a prime factor of the number using the primes list
    answer = 0
    for i in primes:
        if i > 1:
            if number % i == 0:
                answer = i
    return answer

print("Prime Factors Calculator")
print("Enter a number:")

try:
    input_number = int(input())
except:
    print("Please enter a number only.")
    input_number = int(input())
    
number = abs(input_number)

uptoPrimes = 1000
getPrimes(1000)
number1 = number
factors = [1]
spam = [number]

print("Finding factors in primes upto 1000...")
number = calculate(number, uptoPrimes)
    
    
factors.remove(1)
if number != 1:
    factors.append(int(number))

maxFactor = 0
for i in range(len(factors)):
    if maxFactor < len(str(factors[i])):
        maxFactor = len(str(factors[i]))

num_temp = input_number
factors.sort()
multiplicands = [input_number]
for a in range(0,len(factors)-1):
    num_temp = num_temp/factors[a]
    multiplicands.append(int(num_temp))

print()
for a in range(0, len(factors)):
    print(" " * (maxFactor - len(str(factors[a]))) + str(factors[a]),end=" | ")
    print(multiplicands[a])
    print("—"*(maxFactor + 3 + len(str(number1))))
if str(input_number)[0] == "-":
    print(" "*(maxFactor + 1) + "| -1")
else:
    print(" "*(maxFactor + 1) + "| 1")
    
if len(factors) == 1:
    print("This is a prime number.")
