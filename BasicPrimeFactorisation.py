import copy
import math
primes = []

def getPrimes(cheese):
    primeValidator = [True for i in range(cheese)]
    p = 2

    while p**2 <= cheese:
        if primeValidator[p]:
            for a in range(p**2, cheese, p):
                primeValidator[a] = False
        p = p + 1

    for i in range(2,cheese):
        if primeValidator[i]:
            primes.append(i)
        

def multList(anyList):
    result = 1
    for i in anyList:
        result = result * i
    return result

def getFactor(number):
    answer = 0
    for i in primes:
        if i > 1:
            if number % i == 0:
                answer = i
    return answer


print("Prime Factors Calculator")
print("Enter a number:")
number = input()

try:
    number = int(number)
except:
    print("Please enter a number only.")
    number = input()
    

getPrimes(math.floor((number)**0.5) + 1)
number1 = copy.deepcopy(number)
factors = [1]
spam = [number]

while multList(factors) < number1:
    a = getFactor(number)
    if a != 0:
        factors.append(a)
        number = number / a
        spam.append(int(number))
    else:
        factors.append(int(number))
        break
    
    
factors.remove(1)
maxFactor = 0
for i in range(len(factors)):
    if maxFactor < len(str(factors[i])):
        maxFactor = len(str(factors[i]))

print()
for a in range(0, len(factors)):
    print(" " * (maxFactor - len(str(factors[a]))) + str(factors[a]),end=" | ")
    print(spam[a])
    print("—"*(maxFactor + 3 + len(str(number))))

print(" "*(maxFactor + 1) + "| 1")
if len(factors) == 1:
    print("This is a prime number. Yayy!!")
input()
